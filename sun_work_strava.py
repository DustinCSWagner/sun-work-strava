#!/usr/bin/env python3

"""
Created December 2017
@author: Dustin Wagner
Plot times of work, leave, exercise, sunset, and sunrise
Inspired by
http://datadebrief.blogspot.com/2010/10/plotting-sunrise-sunset-times-in-python.html

Procedure ===
First gather your work, leave, and strava hours by date.
I scraped html for my T&A Hours and used Parse_GPX_to_CSV.py from
https://www.ryanbaumann.com/blog/2015/7/18/strava-heat-maps-part-2
for my Strava hours. Final clean up was done in excel.

This file imports xlsx files by date, start time, and stop time.

It calculates sunrise, sunset, dawns, and dusks based on location.
It calculates danger times based on the suns angle above the horizon (+12 deg)
Then it plots:
  - sunrise, sunset, dawns, dusks, and danger times
  - moving average of work hours
  - leave bars
  - strava bars
  - monthly leave hours
  - monthly strava hours

Finally, it displays the plot.
"""

# Imports ===
import astral
import datetime
from dateutil import rrule
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
from matplotlib.ticker import FuncFormatter as ff
import matplotlib.gridspec as gridspec
import numpy as np
import pandas as pd
import pylab
from scipy import signal

# Functions ===
def getSunTimes(dates, place):
    #for list of dates return data frame of: date, sunrise, sunset,
    #astroDawn, astroDusk, nauticalDawn, nauticalDusk, civilDawn, civilDusk,
    #dangerDawn, dangerDusk
    sunrise = []
    sunset = []
    astroDawn = []
    astroDusk = []
    nauticalDawn = []
    nauticalDusk = []
    civilDawn = []
    civilDusk = []
    dangerDawn = []
    dangerDusk = []
    for day in rrule.rrule(rrule.DAILY, dtstart=dates[0], until=dates[-1]):
        sunrise.append(place.time_at_elevation(0, \
            direction=astral.SUN_RISING, date=day.date(), local=True).time())
        sunset.append(place.time_at_elevation(0, \
            direction=astral.SUN_SETTING, date=day.date(), local=True).time())

        astroDawn.append(place.time_at_elevation(-18, \
            direction=astral.SUN_RISING, date=day.date(), local=True).time())
        astroDusk.append(place.time_at_elevation(-18, \
            direction=astral.SUN_SETTING, date=day.date(), local=True).time())

        nauticalDawn.append(place.time_at_elevation(-12, \
            direction=astral.SUN_RISING, date=day.date(), local=True).time())
        nauticalDusk.append(place.time_at_elevation(-12, \
            direction=astral.SUN_SETTING, date=day.date(), local=True).time())

        civilDawn.append(place.time_at_elevation(-6, \
            direction=astral.SUN_RISING, date=day.date(), local=True).time())
        civilDusk.append(place.time_at_elevation(-6, \
            direction=astral.SUN_SETTING, date=day.date(), local=True).time())

        dangerDawn.append(place.time_at_elevation(12, \
            direction=astral.SUN_RISING, date=day.date(), local=True).time())
        dangerDusk.append(place.time_at_elevation(12, \
            direction=astral.SUN_SETTING, date=day.date(), local=True).time())

    df = pd.DataFrame({'date' : dates,
                        'sunrise' : sunrise,
                        'sunset' : sunset,
                        'astroDawn' : astroDawn,
                        'astroDusk' : astroDusk,
                        'nauticalDawn' : nauticalDawn,
                        'nauticalDusk' : nauticalDusk,
                        'civilDawn' : civilDawn,
                        'civilDusk' : civilDusk,
                        'dangerDawn' : dangerDawn,
                        'dangerDusk' : dangerDusk})
    return df

def dt2m(dt):
    return (dt.hour*60) + dt.minute

def m2hm(x, i):
    h = int(x/60)
    m = int(x%60)
    return '%(h)02d:%(m)02d' % {'h':h,'m':m}

def dt2ym(x, pos):
    x = mdates.num2date(x)
    return x.strftime("%Y-%m")

def absolute(x, pos):
    return '%d' % abs(x)

def blank(x, pos):
    return ''

# Parameters ===
norman_ok = astral.Location(info=("Norman, OK", "USA", \
                                  35.22,-97.44, "US/Central", 357))
start_date = datetime.date(2013,11,30)
end_date = datetime.date(2018,1,1)
dates = pd.date_range(start_date, end_date)

sun_times = getSunTimes(dates, norman_ok)
work_hours = pd.read_excel('all_hours.xlsx')
leave_hours = pd.read_excel('leave_hours.xlsx')
strava_hours = pd.read_excel('strava_hours.xlsx')

# Setup data ===
#convert to minutes past midnight
work_hours.Start = work_hours.Start.astype(str)
work_hours.Start = work_hours.Start.str.split(':').apply(lambda x: \
                                             int(x[0]) * 60 + int(x[1]))
work_hours.Stop = work_hours.Stop.astype(str)
work_hours.Stop = work_hours.Stop.str.split(':').apply(lambda x: \
                                           int(x[0]) * 60 + int(x[1]))

leave_hours.Start = leave_hours.Start.astype(str)
leave_hours.Start = leave_hours.Start.str.split(':').apply(lambda x: \
                                               int(x[0]) * 60 + int(x[1]))
leave_hours.Stop = leave_hours.Stop.astype(str)
leave_hours.Stop = leave_hours.Stop.str.split(':').apply(lambda x: \
                                             int(x[0]) * 60 + int(x[1]))

strava_hours.Start = strava_hours.Start.astype(str)
strava_hours.Start = strava_hours.Start.str.split(':').apply(lambda x: \
                                                 int(x[0]) * 60 + int(x[1]))
strava_hours.Stop = strava_hours.Stop.astype(str)
strava_hours.Stop = strava_hours.Stop.str.split(':').apply(lambda x: \
                                               int(x[0]) * 60 + int(x[1]))

sun_times.sunrise = sun_times.sunrise.apply(lambda x: x.minute+60*x.hour)
sun_times.sunset = sun_times.sunset.apply(lambda x: x.minute+60*x.hour)
sun_times.astroDawn = sun_times.astroDawn.apply(lambda x: x.minute+60*x.hour)
sun_times.astroDusk = sun_times.astroDusk.apply(lambda x: x.minute+60*x.hour)
sun_times.nauticalDawn = sun_times.nauticalDawn.apply(lambda x: \
                                                      x.minute+60*x.hour)
sun_times.nauticalDusk = sun_times.nauticalDusk.apply(lambda x: \
                                                      x.minute+60*x.hour)
sun_times.civilDawn = sun_times.civilDawn.apply(lambda x: x.minute+60*x.hour)
sun_times.civilDusk = sun_times.civilDusk.apply(lambda x: x.minute+60*x.hour)
sun_times.dangerDawn = sun_times.dangerDawn.apply(lambda x: x.minute+60*x.hour)
sun_times.dangerDusk = sun_times.dangerDusk.apply(lambda x: x.minute+60*x.hour)

#trim possible empty days
work_hours = work_hours[work_hours.Start != 0]
leave_hours = leave_hours[leave_hours.Start != 0]
strava_hours = strava_hours[strava_hours.Start != 0]

#add smoothed moing averages
#20 work days on average for 4 week month
ma_n = 20
ma_periods=np.ones(ma_n)
len_work_hours = len(work_hours)

wstart = signal.convolve(work_hours.Start, ma_periods) / sum(ma_periods)
wstart = wstart[:len_work_hours]
wstop = signal.convolve(work_hours.Stop, ma_periods) / sum(ma_periods)
wstop = wstop[:len_work_hours]

#elimatite starting bumb by replacing with original un-averaged data
for i in range(ma_n):
    wstart[i]=work_hours.Start[i]
    wstop[i]=work_hours.Stop[i]

work_hours['smooth_start'] = wstart
work_hours['smooth_stop'] = wstop

#monthly hours
strava_hours['duration'] = (strava_hours.Stop-strava_hours.Start)/60
leave_hours['duration'] = (leave_hours.Stop-leave_hours.Start)/60

#fix index
strava_hours.set_index('Date', drop=False, inplace=True)
leave_hours.set_index('Date', drop=False, inplace=True)

#sum monthly
strava_monthly = strava_hours.resample("M").sum()
leave_monthly = leave_hours.resample("M").sum()
#replace nan with zero
strava_monthly['duration'].fillna(0, inplace=True)
leave_monthly['duration'].fillna(0, inplace=True)
#drop months earlier than start date
leave_monthly = leave_monthly[leave_monthly.index >= \
                              pd.DatetimeIndex([start_date])[0]]

# Layout figures ===
fig = plt.figure()
plt.style.use('ggplot')

gs = gridspec.GridSpec(2, 1, height_ratios=[4, 1])
ax1 = plt.subplot(gs[0]) #daily data
ax2 = plt.subplot(gs[1]) #montly data

#ax1 - daily data
ax1.set_xlim(start_date,dates[-1])
ax1.xaxis.set_major_locator(mdates.MonthLocator(bymonthday=1, interval=3))
locator = mdates.DayLocator()
locator.MAXTICKS = 5000
ax1.xaxis.set_minor_locator(locator)
ax1.xaxis.set_major_formatter(ff(blank))
ax1.xaxis.grid(True)
ax1.xaxis.grid(linestyle='--', linewidth=0.5)

ax1.set_ylim(3*60,23*60) #3am to 11pm
ax1.yaxis.set_major_locator(pylab.MultipleLocator(60))
ax1.yaxis.set_minor_locator(pylab.MultipleLocator(15))
ax1.yaxis.set_major_formatter(ff(m2hm))
ax1.yaxis.grid(linestyle='--', linewidth=0.5)
ax1.set_ylabel('Hour of Day')

#ax2 - monthly data
ax2.set_xlim(start_date,dates[-1])
ax2.xaxis.set_major_locator(mdates.MonthLocator(bymonthday=1, interval=3))
ax2.xaxis.set_minor_locator(locator)
ax2.xaxis.set_major_formatter(ff(dt2ym))
ax2.xaxis.grid(True)
ax2.xaxis.grid(linestyle='--', linewidth=0.5)

#ax2.yaxis.tick_right()
#ax2.yaxis.set_label_position("right")
ax2.set_ylim(-60,120)
ax2.set_ylabel("Monthly Hours")
ax2.yaxis.set_major_formatter(ff(absolute))
ax2.yaxis.grid(linestyle='-.', linewidth=0.5)

labels = ax2.get_xticklabels()
for label in labels:
    label.set_rotation(90)

plt.xlabel('Date')


# Plot data ===
[dusk_astro] = ax1.plot_date(sun_times.date, sun_times.astroDusk, ':', \
             label=' Astronomical Dawn/Dusk', color='#004075')
[dusk_nautical] = ax1.plot_date(sun_times.date, sun_times.nauticalDusk, ':', \
             label=' Nautical Dawn/Dusk', color='#005499')
[dusk_civil] = ax1.plot_date(sun_times.date, sun_times.civilDusk, ':', \
             label=' Civil Dawn/Dusk', color='#006dc6')
[sunset] = ax1.plot_date(sun_times.date, sun_times.sunset, linestyle='-', \
             marker=None, label=' Sunrise/Sunset', color='#fc957b')
[dusk_danger] = ax1.plot_date(sun_times.date, sun_times.dangerDusk, \
             linestyle='-', marker=None, label=' Danger Zone - Sun in eyes', \
             color='#fffdaf')
ax1.plot_date(sun_times.date, sun_times.dangerDawn, linestyle='-', \
             marker=None, label='_no_label_', color='#fffdaf')
ax1.plot_date(sun_times.date, sun_times.sunrise, linestyle='-', \
             marker=None, label='_no_label_', color='#fc957b')
ax1.plot_date(sun_times.date, sun_times.civilDawn, ':', \
             label='_no_label_', color='#006dc6')
ax1.plot_date(sun_times.date, sun_times.nauticalDawn, ':', \
             label='_no_label_', color='#005499')
ax1.plot_date(sun_times.date, sun_times.astroDawn, ':', \
             label='_no_label_', color='#004075')

leave = ax1.vlines(list(leave_hours.Date), leave_hours.Stop, \
                   leave_hours.Start, lw=2, color='white', \
                   label=' Leave Hours', alpha=0.8)

[work] = ax1.plot_date(work_hours.Date, work_hours.smooth_start, \
             linestyle='-', lw=1, marker=None, \
             label=' Moving Average Start/Stop Work', color='black', alpha=0.5)

ax1.plot_date(work_hours.Date, work_hours.smooth_stop, linestyle='-', \
             lw=1, marker=None, label='_no_label_', color='black', \
             alpha=0.5)

strava = ax1.vlines(list(strava_hours.Date), strava_hours.Stop, \
                    strava_hours.Start, lw=2, color='orange', \
                    label=' Strava Hours', alpha=0.8)

ax1.fill_between(list(work_hours.Date), work_hours.smooth_start, \
                work_hours.smooth_stop, facecolor='black', alpha=0.25)

ax1.fill_between(list(sun_times.date), sun_times.dangerDusk, \
                sun_times.sunset, color='#fffdaf', \
                facecolor='#fffdaf', alpha=0.5)
ax1.fill_between(list(sun_times.date), sun_times.sunrise, \
                sun_times.dangerDawn, color='#fffdaf', \
                facecolor='#fffdaf', alpha=0.5)

ax2.bar(leave_monthly.index, leave_monthly.duration, width=20, color="white")
ax2.bar(strava_monthly.index, -1*strava_monthly.duration, width=20, \
        color="orange")

# Legend, Title, and Layout ===
#ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
#ax1.legend()
legend = plt.legend(handles=[dusk_astro,dusk_nautical,dusk_civil,sunset,\
                    dusk_danger, work, leave, strava], bbox_to_anchor=(1, 1), \
                    loc='lower left')

plt.gca().add_artist(legend)

title_string = "Work, Leave, and Exercise vs Sunrise and Sunset"
subtitle_string = str(start_date.year+1) + " through " + \
    str(end_date.year-1) +  " - Norman, OK \n"

plt.suptitle(title_string, y=1.05, fontsize=18)
ax1.set_title(subtitle_string, y=1.025, fontsize=12)

# Ouput ===
fig.set_size_inches(11,8.5)
fig.set_dpi(300)
fig.tight_layout(pad=0.0)

[__.set_clip_on(False) for __ in plt.gca().get_children()]
plt.show()

#figure_path = 'sun_work_strava.png'
#fig.savefig(figure_path, dpi=fig.dpi, bbox_inches='tight', pad_inches=0)

